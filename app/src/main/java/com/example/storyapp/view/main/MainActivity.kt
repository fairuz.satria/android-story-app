package com.example.storyapp.view.main

import android.content.Context
import android.content.Intent
import android.content.pm.ActivityInfo
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import androidx.core.view.isVisible
import androidx.datastore.core.DataStore
import androidx.datastore.preferences.core.Preferences
import androidx.datastore.preferences.preferencesDataStore
import androidx.lifecycle.ViewModelProvider
import androidx.paging.PagingData
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.storyapp.R
import com.example.storyapp.data.local.entity.ListStoryItem
import com.example.storyapp.data.local.preferences.UserModel
import com.example.storyapp.databinding.ActivityMainBinding
import com.example.storyapp.view.ViewModelFactory
import com.example.storyapp.view.login.LoginActivity
import com.example.storyapp.di.Injection
import com.example.storyapp.view.adapter.LoadingStateAdapter
import com.example.storyapp.view.adapter.StoryAdapter
import com.example.storyapp.view.add.AddStoryActivity
import com.example.storyapp.view.map.MapsActivity

private val Context.dataStore: DataStore<Preferences> by preferencesDataStore(name = "settings")

class MainActivity : AppCompatActivity() {
    private lateinit var mainViewModel: MainViewModel
    private lateinit var binding: ActivityMainBinding
    private lateinit var user: UserModel
    private lateinit var adapter: StoryAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)


        setupView()
        setupViewModel()
        setupAction()
        setFab()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.logout -> {
               mainViewModel.logout()
                return true
            }
            R.id.maps -> {
                val intentMaps = Intent(applicationContext, MapsActivity::class.java)
//                intentDetail.putExtra("user", user)
                startActivity(intentMaps)
                return true
            }
            else -> return true
        }
    }

    override fun onResume() {
        super.onResume()
        adapter.refresh()
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.option_menu, menu)

        return true
    }

//    private fun setListStory(listStory: List<ListStoryItem>) {
//        val adapter = StoryAdapter(listStory)
//        binding.rvStory.adapter = adapter
//    }


    private fun setupView() {
        val layoutManager = LinearLayoutManager(this)
        binding.rvStory.layoutManager = layoutManager
        adapter = StoryAdapter()
        binding.rvStory.adapter = adapter.withLoadStateFooter(
            footer = LoadingStateAdapter {
                adapter.retry()
            }
        )

    }

    private fun setupViewModel() {
        mainViewModel = ViewModelProvider(
            this,
            ViewModelFactory(Injection.provideRepository(applicationContext, dataStore))
        )[MainViewModel::class.java]

        mainViewModel.story?.observe(this, {
            binding.swipeRefreshLayout.isRefreshing = false
            Log.e("main activity", it.toString())
            adapter.submitData(lifecycle, it)
        })

        mainViewModel.getUser().observe(this, { user ->
            if (user.isLogin){
                Log.e("MainActivity", "login")
                this.user = user
            } else {
                Log.e("MainActivity", "not login")
                val intent = Intent(this, LoginActivity::class.java)
                intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK
                startActivity(intent)
            }
        })
    }



    private fun setupAction() {
        binding.swipeRefreshLayout.setOnRefreshListener {
            adapter.refresh()
        }
    }

    private fun setFab(){
        binding.fab.setOnClickListener({
            val intentDetail = Intent(applicationContext, AddStoryActivity::class.java)
            intentDetail.putExtra("user", user)
            startActivity(intentDetail)
        })
    }

    private fun playAnimation() {
        TODO("create animation")
    }
}
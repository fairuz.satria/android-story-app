package com.example.storyapp.view.map

import android.util.Log
import androidx.core.view.isVisible
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.storyapp.data.Repository
import com.example.storyapp.data.local.entity.ListStoryItem
import com.example.storyapp.data.remote.response.ListStoryResponse
import com.example.storyapp.data.remote.response.LoginResponse
import com.google.gson.Gson
import kotlinx.coroutines.runBlocking
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MapsViewModel(private val repo: Repository) : ViewModel() {
    private val _listStoryResponse = MutableLiveData<ListStoryResponse>()
    val listStoryResponse: LiveData<ListStoryResponse> = _listStoryResponse

    private val _isLoading = MutableLiveData<Boolean>()
    val isLoading: LiveData<Boolean> = _isLoading

    fun getStory(size: Int) = runBlocking {
        _isLoading.value = true
        repo.getAllStoryWLocationAndSize(size).enqueue(object : Callback<ListStoryResponse> {
            override fun onResponse(
                call: Call<ListStoryResponse>,
                response: Response<ListStoryResponse>,
            ) {
                _isLoading.value = false
                Log.e("MapsViewModel", Gson().toJson(response.body()))
                if(response.isSuccessful){
                    _listStoryResponse.value = response.body()
                }else{
                    val listResponse = Gson().fromJson(response.errorBody()?.string(), ListStoryResponse::class.java)
                    _listStoryResponse.value = listResponse
                }
            }

            override fun onFailure(call: Call<ListStoryResponse>, t: Throwable) {
                _isLoading.value = false
            }

        })
    }


}
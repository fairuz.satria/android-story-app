package com.example.storyapp.view.map

import android.content.Context
import android.graphics.Bitmap
import android.location.Geocoder
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.isVisible
import androidx.datastore.core.DataStore
import androidx.datastore.preferences.core.Preferences
import androidx.datastore.preferences.preferencesDataStore
import androidx.lifecycle.ViewModelProvider
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.example.storyapp.R
import com.example.storyapp.data.local.entity.ListStoryItem
import com.example.storyapp.databinding.ActivityMapsBinding
import com.example.storyapp.di.Injection
import com.example.storyapp.view.ViewModelFactory
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.LatLngBounds
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions
import java.io.IOException
import java.util.*


private val Context.dataStore: DataStore<Preferences> by preferencesDataStore(name = "settings")

class MapsActivity : AppCompatActivity(), OnMapReadyCallback, GoogleMap.InfoWindowAdapter {

    private lateinit var mMap: GoogleMap
    private lateinit var binding: ActivityMapsBinding
    private lateinit var viewModel: MapsViewModel
    private val boundsBuilder = LatLngBounds.Builder()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)


        binding = ActivityMapsBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val mapFragment = supportFragmentManager
            .findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)
    }

    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap

        mMap.setInfoWindowAdapter(this)
        setupViewModel()
    }


    private fun setupViewModel() {
        viewModel = ViewModelProvider(
            this,
            ViewModelFactory(Injection.provideRepository(applicationContext, dataStore))
        )[MapsViewModel::class.java]

        viewModel.getStory(1000)

        viewModel.listStoryResponse.observe(this, {
            it.listStory.forEach({
                addMarker(it)
            })
            buildBounds()
        })

        viewModel.isLoading.observe(this, {
            showLoading(it)
        })
    }

    private fun addMarker(story: ListStoryItem) {
        if(story.lat < 90 && story.lat > -90) {
            Log.d("MapsActivity", story.lat.toString() + " " + story.lon.toString())
            val latLng = LatLng(story.lat, story.lon)
            val addressName = getAddressName(story.lat, story.lon)
            val marker: Marker = mMap.addMarker(MarkerOptions().position(latLng).snippet(addressName).title(story.name))!!
            marker.tag = story

            boundsBuilder.include(latLng)
        }
    }

    private fun buildBounds() {
        val bounds: LatLngBounds = boundsBuilder.build()
        mMap.animateCamera(
            CameraUpdateFactory.newLatLngBounds(
                bounds,
                resources.displayMetrics.widthPixels,
                resources.displayMetrics.heightPixels,
                300
            )
        )
    }

    private fun getAddressName(lat: Double, lon: Double): String? {
        var addressName: String? = null
        val geocoder = Geocoder(this@MapsActivity, Locale.getDefault())
        try {
            val list = geocoder.getFromLocation(lat, lon, 1)
            if (list != null && list.size != 0) {
                addressName = list[0].getAddressLine(0)
            }
        } catch (e: IOException) {
            e.printStackTrace()
        }
        return addressName
    }

    private fun showLoading(it: Boolean) {
        binding.progressBar.isVisible = it
    }

    companion object {
        private const val TAG = "MapsActivity"
    }

    override fun getInfoWindow(marker: Marker): View? {
        return null // Return null to use default rendering
    }

    override fun getInfoContents(marker: Marker): View {
        val view = layoutInflater.inflate(R.layout.info_window_layout, null)
        val imageView = view.findViewById<ImageView>(R.id.imageInfo)
        val descriptionTextView = view.findViewById<TextView>(R.id.descriptionTextView)

        val story = marker.tag as ListStoryItem

        descriptionTextView.text = story.description
        Log.e("info window", story.photoUrl)

        Glide.with(this).asBitmap().load(story.photoUrl)
            .listener(object : RequestListener<Bitmap?> {

                override fun onResourceReady(
                    resource: Bitmap?,
                    model: Any?,
                    target: com.bumptech.glide.request.target.Target<Bitmap?>?,
                    dataSource: DataSource?,
                    isFirstResource: Boolean,
                ): Boolean {
                    Log.e("glide info window", marker.isInfoWindowShown.toString())
                    if (marker!!.isInfoWindowShown) {
                        marker!!.hideInfoWindow()
                        marker!!.showInfoWindow()
                    }
                    return false
                }

                override fun onLoadFailed(
                    e: GlideException?,
                    model: Any?,
                    target: com.bumptech.glide.request.target.Target<Bitmap?>?,
                    isFirstResource: Boolean,
                ): Boolean {
                    return false
                }
            }).into(imageView)

        return view
    }

}
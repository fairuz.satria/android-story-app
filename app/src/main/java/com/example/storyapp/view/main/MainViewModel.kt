package com.example.storyapp.view.main

import android.util.Log
import androidx.lifecycle.*
import androidx.paging.PagingData
import androidx.paging.cachedIn
import com.example.storyapp.data.Repository
import com.example.storyapp.data.local.entity.ListStoryItem
import com.example.storyapp.data.local.preferences.UserModel
import com.example.storyapp.data.local.room.StoryDatabase
import com.example.storyapp.data.remote.response.ListStoryResponse
import com.google.gson.Gson
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MainViewModel(private val repo: Repository) : ViewModel() {

    private val _isLoading = MutableLiveData<List<Boolean>>()
    val isLoading: LiveData<List<Boolean>> = _isLoading

    fun getUser(): LiveData<UserModel> {
        _isLoading.value = mutableListOf(true, false)
        return repo.getUser().asLiveData()
    }

//    fun loadAllStory(token: String) {
//        _isLoading.value = mutableListOf(true, false)
//        repo.getAllStory(token).enqueue(object : Callback<ListStoryResponse> {
//            override fun onResponse(
//                call: Call<ListStoryResponse>,
//                response: Response<ListStoryResponse>
//            ) {
//                if(response.isSuccessful){
//                    _listStoryResponse.value = response.body()
//                    _isLoading.value = mutableListOf(false, true)
//                }else{
//                    val errorBody = response.errorBody()?.string()
//                    _listStoryResponse.value = Gson().fromJson(errorBody, ListStoryResponse::class.java)
//                    _isLoading.value = mutableListOf(false, false)
//                }
//            }
//
//            override fun onFailure(call: Call<ListStoryResponse>, t: Throwable) {
//
//            }
//        })
//    }
    var story: LiveData<PagingData<ListStoryItem>> = repo.getAllStory().cachedIn(viewModelScope)


    fun logout() {
        viewModelScope.launch {
            Log.e("test", "test")
            repo.deleteUser()
        }
    }

}
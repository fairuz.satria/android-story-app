package com.example.storyapp.view.login

import android.util.Log
import androidx.lifecycle.*
import com.example.storyapp.data.Repository
import com.example.storyapp.data.local.preferences.UserModel
import com.example.storyapp.data.remote.response.LoginResponse
import com.google.gson.Gson
import kotlinx.coroutines.launch
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class LoginViewModel(private val repo: Repository) : ViewModel() {

    private val _loginResponse = MutableLiveData<LoginResponse>()
    val loginResponse: LiveData<LoginResponse> = _loginResponse

    private val _isLoading = MutableLiveData<Boolean>()
    val isLoading: LiveData<Boolean> = _isLoading

    fun getUser(): LiveData<UserModel> {
        return repo.getUser().asLiveData()
    }

    fun login(email: String, password: String) {
        _isLoading.value = true
        repo.login(email, password).enqueue(object : Callback<LoginResponse> {
            override fun onResponse(
                call: Call<LoginResponse>,
                response: Response<LoginResponse>
            ) {

                _isLoading.value = false
                if(response.isSuccessful){
                    val loginResponse = response.body()
                    _loginResponse.value = response.body()

                    viewModelScope.launch {
                        response.body()?.loginResult?.let { repo.saveUser(it) }
                    }
                }else{
                    val loginResponse = Gson().fromJson(response.errorBody()?.string(), LoginResponse::class.java)
                    _loginResponse.value = loginResponse

                }


            }

            override fun onFailure(call: Call<LoginResponse>, t: Throwable) {
               val loginResponse = LoginResponse(null, true, "Server Failure")
                Log.e("test", "onFailure")
                _isLoading.value = false
                _loginResponse.value = loginResponse
            }
        })
    }
}
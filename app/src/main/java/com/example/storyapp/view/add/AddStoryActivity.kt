package com.example.storyapp.view.add

import android.Manifest
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.Intent.ACTION_GET_CONTENT
import android.content.pm.ActivityInfo
import android.content.pm.PackageManager
import android.graphics.BitmapFactory
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.provider.MediaStore
import android.util.Log
import android.widget.Toast
import androidx.activity.result.ActivityResultLauncher
import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.app.AlertDialog
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.core.content.FileProvider
import androidx.core.view.isVisible
import androidx.datastore.core.DataStore
import androidx.datastore.preferences.core.Preferences
import androidx.datastore.preferences.preferencesDataStore
import androidx.lifecycle.ViewModelProvider
import com.example.storyapp.R
import com.example.storyapp.createCustomTempFile
import com.example.storyapp.data.local.preferences.UserModel
import com.example.storyapp.databinding.ActivityAddStoryBinding
import com.example.storyapp.di.Injection
import com.example.storyapp.uriToFile
import com.example.storyapp.view.ViewModelFactory
import java.io.File

private val Context.dataStore: DataStore<Preferences> by preferencesDataStore(name = "settings")

class AddStoryActivity : AppCompatActivity() {
    companion object {
        private val REQUIRED_PERMISSIONS = arrayOf(Manifest.permission.CAMERA)
        private const val REQUEST_CODE_PERMISSIONS = 10
    }
    private lateinit var user: UserModel
    private lateinit var addViewModel: AddViewModel
    private var getFile: File? = null
    private lateinit var binding: ActivityAddStoryBinding
    private lateinit var currentPhotoPath: String
    private var lat: Double? = null
    private var lon: Double? = null
    private var address: String? = null
    private val launcherIntentCamera = registerForActivityResult(
        ActivityResultContracts.StartActivityForResult()
    ) {
        if (it.resultCode == RESULT_OK) {
            val myFile = File(currentPhotoPath)

            myFile.let { file ->
                getFile = file
                binding.placeImage.setImageBitmap(BitmapFactory.decodeFile(file.path))
            }
        }
    }
    private val launcherIntentGallery = registerForActivityResult(
        ActivityResultContracts.StartActivityForResult()
    ) { result ->
        if (result.resultCode == RESULT_OK) {
            val selectedImg = result.data?.data as Uri
            selectedImg.let { uri ->
                val myFile = uriToFile(uri, this@AddStoryActivity)
                getFile = myFile
                binding.placeImage.setImageURI(uri)
            }
        }
    }
    private var resultLauncher: ActivityResultLauncher<Intent> = registerForActivityResult(
        ActivityResultContracts.StartActivityForResult()
    ) { result ->
        if (result.resultCode == Activity.RESULT_OK) {
            val data: Intent? = result.data
            lat = data?.getDoubleExtra("lat",0.0)
            lon = data?.getDoubleExtra("lon", 0.0)
            binding.address.text = data?.getStringExtra("address")
        }
    }
    private fun allPermissionsGranted() = REQUIRED_PERMISSIONS.all {
        ContextCompat.checkSelfPermission(baseContext, it) == PackageManager.PERMISSION_GRANTED
    }




    override fun onCreate(savedInstanceState: Bundle?) {
        requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
        super.onCreate(savedInstanceState)
        binding = ActivityAddStoryBinding.inflate(layoutInflater)
        setContentView(binding.root)


        getParceableUser()
        setupViewModel()
        setupAction()
    }

    private fun getParceableUser() {
        user = intent.getParcelableExtra<UserModel>("user")!!
    }

    private fun setupViewModel() {
        addViewModel = ViewModelProvider(
            this,
            ViewModelFactory(Injection.provideRepository(application, dataStore))
        )[AddViewModel::class.java]

        addViewModel.fileUploadResponseResponse.observe(this, {
            if(it.error!!){
                AlertDialog.Builder(this).apply {
                    setTitle("Maaf terjadi kesalahan")
                    setMessage(it.message)
                    setPositiveButton("kembali"){ dialog, which ->
                        dialog.dismiss()
                    }
                    create()
                    show()
                }
            }else{
                getFile = null
                binding.placeImage.setImageResource(R.drawable.ic_place_holder)
                binding.descInput.setText("")
                AlertDialog.Builder(this).apply {
                    setTitle("Berhasil")
                    setMessage(it.message)
                    setPositiveButton("kembali"){ dialog, which ->
                        finish()
                    }
                    create()
                    show()
                }
            }
        })

        addViewModel.isLoading.observe(this, {
            showLoading(it)
        })
    }

    private fun showLoading(isLoading: Boolean) {
        binding.uploadButton.isClickable = !isLoading
        binding.uploadButton.isVisible = !isLoading
        binding.progressBar.isVisible = isLoading
    }

    private fun setupAction() {
        binding.cameraButton.setOnClickListener {
            startTakePhoto()
        }
        binding.galleryButton.setOnClickListener {
            startGallery()
        }
        binding.uploadButton.setOnClickListener {
            uploadImage()
        }
        binding.locationButton.setOnClickListener {
            val intentMaps = Intent(applicationContext, MapsLocationPickActivity::class.java)

            if(lat != null && lon != null){
                Log.d("AddStoryActivit", lat!!.toString())
                Log.d("AddStoryActivit", lon!!.toString())
                intentMaps.putExtra("lat", lat.toString())
                intentMaps.putExtra("lon", lon.toString())
            }
            resultLauncher.launch(intentMaps)
        }
    }


    private fun startGallery() {
        val intent = Intent()
        intent.action = ACTION_GET_CONTENT
        intent.type = "image/*"
        val chooser = Intent.createChooser(intent, "Choose a Picture")
        launcherIntentGallery.launch(chooser)
    }

    private fun startTakePhoto() {
        if (!allPermissionsGranted()) {
            ActivityCompat.requestPermissions(
                this,
                REQUIRED_PERMISSIONS,
                REQUEST_CODE_PERMISSIONS
            )
        }else{
            val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
            intent.resolveActivity(packageManager)

            createCustomTempFile(application).also {
                val photoURI: Uri = FileProvider.getUriForFile(
                    this@AddStoryActivity,
                    "com.example.storyapp",
                    it
                )
                currentPhotoPath = it.absolutePath
                intent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI)
                launcherIntentCamera.launch(intent)
            }
        }
    }

    private fun uploadImage() {
        val description = binding.descInput.text.toString()
        when{
            description.isEmpty() -> {
                binding.descInput.error = "Masukkan deskripsi"
            }
            getFile == null -> {
                Toast.makeText(this@AddStoryActivity, "Silakan masukkan berkas gambar terlebih dahulu.", Toast.LENGTH_SHORT).show()
            }
            lat != null && lon != null -> {
                addViewModel.uploadStoryWLocation("Bearer " + user.token, getFile!!, description, lat!!, lon!!)
            }
            else -> {
                addViewModel.uploadStory("Bearer " + user.token, getFile!!, description)
            }
        }

    }


    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == REQUEST_CODE_PERMISSIONS) {
            if (allPermissionsGranted()) {
                startTakePhoto()
            } else {
                Toast.makeText(this, "Permissions not granted by the user.", Toast.LENGTH_SHORT).show()
            }
        }
    }
}
package com.example.storyapp.view.detail

import android.content.pm.ActivityInfo
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.bumptech.glide.Glide
import com.example.storyapp.data.local.entity.ListStoryItem
import com.example.storyapp.databinding.ActivityDetailStoryBinding

class DetailStoryActivity : AppCompatActivity() {
    private lateinit var binding: ActivityDetailStoryBinding
    private lateinit var story: ListStoryItem


    override fun onCreate(savedInstanceState: Bundle?) {
        requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
        super.onCreate(savedInstanceState)
        binding = ActivityDetailStoryBinding.inflate(layoutInflater)
        setContentView(binding.root)


        getParcelable()
        setView()
    }

    private fun getParcelable() {
        story = intent.getParcelableExtra<ListStoryItem>("story")!!
    }

    private fun setView(){
        val (id, photoUrl, createdAt, name, desc) = story
        Glide.with(applicationContext)
            .load(photoUrl)
            .into(binding.placeImage)
        binding.placeName.text = name
        binding.placeDescription.text = desc
    }
}
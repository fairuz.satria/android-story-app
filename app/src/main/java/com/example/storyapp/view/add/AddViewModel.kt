package com.example.storyapp.view.add

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.storyapp.data.Repository
import com.example.storyapp.data.remote.response.FileUploadResponse
import com.example.storyapp.reduceFileImage
import com.google.gson.Gson
import okhttp3.MediaType.Companion.toMediaType
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.MultipartBody
import okhttp3.RequestBody
import okhttp3.RequestBody.Companion.asRequestBody
import okhttp3.RequestBody.Companion.toRequestBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.File

class AddViewModel(private val repo: Repository): ViewModel() {

    private val _fileUploadResponse = MutableLiveData<FileUploadResponse>()
    val fileUploadResponseResponse: LiveData<FileUploadResponse> = _fileUploadResponse

    private val _isLoading = MutableLiveData<Boolean>()
    val isLoading: LiveData<Boolean> = _isLoading

    fun uploadStory(token: String, image: File, description: String){
        _isLoading.value = true

        val file = reduceFileImage(image)

        val description = description.toRequestBody("text/plain".toMediaType())
        val requestImageFile = file.asRequestBody("image/jpeg".toMediaTypeOrNull())
        val imageMultipart: MultipartBody.Part = MultipartBody.Part.createFormData(
            "photo",
            file.name,
            requestImageFile
        )

        repo.uploadStory(token, imageMultipart, description).enqueue(object: Callback<FileUploadResponse> {
            override fun onResponse(
                call: Call<FileUploadResponse>,
                response: Response<FileUploadResponse>,
            ) {
                _isLoading.value = false
                if(response.isSuccessful){
                    _fileUploadResponse.value = response.body()
                }else{
                    val fileUploadResponseErr = Gson().fromJson(response.errorBody()?.string(), FileUploadResponse::class.java)
                    _fileUploadResponse.value = fileUploadResponseErr
                }
            }

            override fun onFailure(call: Call<FileUploadResponse>, t: Throwable) {
                _isLoading.value = false
                _fileUploadResponse.value = FileUploadResponse(true, "Server Failure")
            }

        })
    }

    fun uploadStoryWLocation(token: String, image: File, description: String, lat: Double, lon: Double) {
        _isLoading.value = true

        val file = reduceFileImage(image)

        val description = description.toRequestBody("text/plain".toMediaType())
        val lat = RequestBody.create("text/plain".toMediaTypeOrNull(), lat.toString());
        val lon = RequestBody.create("text/plain".toMediaTypeOrNull(), lon.toString());
        val requestImageFile = file.asRequestBody("image/jpeg".toMediaTypeOrNull())
        val imageMultipart: MultipartBody.Part = MultipartBody.Part.createFormData(
            "photo",
            file.name,
            requestImageFile
        )

        repo.uploadStoryWLocation(token, imageMultipart, description, lat, lon).enqueue(object: Callback<FileUploadResponse> {
            override fun onResponse(
                call: Call<FileUploadResponse>,
                response: Response<FileUploadResponse>,
            ) {
                _isLoading.value = false
                if(response.isSuccessful){
                    _fileUploadResponse.value = response.body()
                }else{
                    val fileUploadResponseErr = Gson().fromJson(response.errorBody()?.string(), FileUploadResponse::class.java)
                    _fileUploadResponse.value = fileUploadResponseErr
                }
            }

            override fun onFailure(call: Call<FileUploadResponse>, t: Throwable) {
                _isLoading.value = false
                _fileUploadResponse.value = FileUploadResponse(true, "Server Failure")
            }

        })
    }

}
package com.example.storyapp.data.remote.retrofit

import com.example.storyapp.data.remote.request.LoginRequest
import com.example.storyapp.data.remote.request.RegisterRequest
import com.example.storyapp.data.remote.response.FileUploadResponse
import com.example.storyapp.data.remote.response.ListStoryResponse
import com.example.storyapp.data.remote.response.LoginResponse
import com.example.storyapp.data.remote.response.RegisterResponse
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.http.*

interface ApiService {
    @POST("/v1/login")
    fun login(@Body loginRequest: LoginRequest): Call<LoginResponse>

    @POST("/v1/register")
    fun register(@Body registerRequest: RegisterRequest): Call<RegisterResponse>

    @GET("/v1/stories")
    suspend fun getAllStories(@Header("Authorization") token: String,
                      @Query("page") page: Int,
                      @Query("size") size: Int): ListStoryResponse

    @GET("/v1/stories")
    fun getAllStoriesWLocation(@Header("Authorization") token: String,
                                @Query("location") location: Int,
                                @Query("size") size: Int): Call<ListStoryResponse>

    @Multipart
    @POST("/v1/stories")
    fun uploadImage(
        @Header("Authorization") token: String,
        @Part file: MultipartBody.Part,
        @Part("description") description: RequestBody,
    ): Call<FileUploadResponse>

    @Multipart
    @POST("/v1/stories")
    fun uploadImageWLocation(
        @Header("Authorization") token: String,
        @Part file: MultipartBody.Part,
        @Part("description") description: RequestBody,
        @Part("lat") lat: RequestBody,
        @Part("lon") lon: RequestBody,
    ): Call<FileUploadResponse>
}
package com.example.storyapp.data.local.preferences

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class UserModel(
    val name: String,
    val userId: String,
    val token: String,
    val isLogin: Boolean
): Parcelable
package com.example.storyapp.data.remote.response

import kotlinx.parcelize.Parcelize
import android.os.Parcelable
import com.example.storyapp.data.local.entity.ListStoryItem
import com.google.gson.annotations.SerializedName

@Parcelize
data class LoginResponse(

	@field:SerializedName("loginResult")
	var loginResult: LoginResult? = null,

	@field:SerializedName("error")
	var error: Boolean? = null,

	@field:SerializedName("message")
	var message: String? = null
) : Parcelable

@Parcelize
data class LoginResult(

	@field:SerializedName("name")
	val name: String,

	@field:SerializedName("userId")
	val userId: String,

	@field:SerializedName("token")
	val token: String
) : Parcelable

@Parcelize
data class RegisterResponse(

	@field:SerializedName("error")
	val error: Boolean? = null,

	@field:SerializedName("message")
	val message: String? = null
) : Parcelable

@Parcelize
data class FileUploadResponse(

	@field:SerializedName("error")
	val error: Boolean? = null,

	@field:SerializedName("message")
	val message: String? = null
) : Parcelable

@Parcelize
data class ListStoryResponse(

	@field:SerializedName("listStory")
	var listStory: List<ListStoryItem>,

	@field:SerializedName("error")
	var error: Boolean,

	@field:SerializedName("message")
	var message: String
) : Parcelable


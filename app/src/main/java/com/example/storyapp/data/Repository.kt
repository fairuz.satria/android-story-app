package com.example.storyapp.data

import android.util.Log
import androidx.datastore.core.DataStore
import androidx.datastore.preferences.core.Preferences
import androidx.lifecycle.LiveData
import androidx.paging.*
import com.example.storyapp.data.local.entity.ListStoryItem
import com.example.storyapp.data.local.preferences.UserModel
import com.example.storyapp.data.local.preferences.UserPreference
import com.example.storyapp.data.local.room.StoryDatabase
import com.example.storyapp.data.remote.request.LoginRequest
import com.example.storyapp.data.remote.request.RegisterRequest
import com.example.storyapp.data.remote.response.*
import com.example.storyapp.data.remote.retrofit.ApiService
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.first
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Call


class Repository private constructor(
    private val database: StoryDatabase,
    private val apiService: ApiService,
    private val userPreference: UserPreference
){

    fun register(name: String, email: String, password: String): Call<RegisterResponse> {
        val resgisterRequest = RegisterRequest(name, email, password)
        return apiService.register(resgisterRequest)
    }

    fun uploadStory(token: String, file: MultipartBody.Part, description: RequestBody): Call<FileUploadResponse>{
        return apiService.uploadImage(token, file, description)
    }

    fun uploadStoryWLocation(token: String, file: MultipartBody.Part, description: RequestBody, lat: RequestBody, lon: RequestBody): Call<FileUploadResponse>{
        return apiService.uploadImageWLocation(token, file, description, lat, lon)
    }

    fun getUser(): Flow<UserModel> {
        return userPreference.getUser();
    }

    suspend fun getAllStoryWLocationAndSize(size: Int): Call<ListStoryResponse> {
        return apiService.getAllStoriesWLocation("Bearer " + userPreference.getUser().first().token, 1, size)
    }

    fun getAllStory(): LiveData<PagingData<ListStoryItem>>{
        Log.e("", "getAllStory")
        @OptIn(ExperimentalPagingApi::class)
        return Pager(
            config = PagingConfig(
                pageSize = 5
            ),
            remoteMediator = StoryRemoteMediator(database, apiService, userPreference),
            pagingSourceFactory = {
                database.storyDao().getAllStory()
            }
        ).liveData
    }


    suspend fun saveUser(loginRes: LoginResult) {
        val user = UserModel(loginRes.name, loginRes.userId, loginRes.token, true)
        userPreference.saveUser(user)
    }

    suspend fun deleteUser() {
        userPreference.deleteUser()
    }

    fun login(name: String, password: String): Call<LoginResponse> {
        val loginRequest = LoginRequest(name, password)
        return apiService.login(loginRequest)
    }


    companion object {
        @Volatile
        private var instance: Repository? = null
        fun getInstance(
            database: StoryDatabase,
            apiService: ApiService,
            dataStore: DataStore<Preferences>
        ): Repository =
            instance ?: synchronized(this) {
                instance ?: Repository(database, apiService, UserPreference.getInstance(dataStore))
            }.also { instance = it }
    }
}
package com.example.storyapp

import com.example.storyapp.data.local.entity.ListStoryItem

object DataDummy {

    fun generateDummyResponse(): List<ListStoryItem> {
        val items: MutableList<ListStoryItem> = arrayListOf()
        for (i in 0..100) {
            val quote = ListStoryItem(
                i.toString(),
                "photoUrl $i",
                "createdAt $i",
                "name $i",
                "description $i",
                i.toDouble(),
                i.toDouble()
            )
            items.add(quote)
        }
        return items
    }
}